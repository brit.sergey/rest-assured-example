package org.brit.tests;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import org.apache.commons.lang3.RandomStringUtils;
import org.brit.tests.classes.StatusEnum;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.ThreadLocalRandom;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by sbryt on 8/26/2016.
 */

/**
 * Just pure rest-assured tests
 */
public class PetTests extends BaseTestClass {

    private Integer petId = ThreadLocalRandom.current().nextInt(1000000, 9999999);
    private String petName = "MyLittlePet_" + RandomStringUtils.randomAlphanumeric(5);
    private String newPetName = "NewName_" + RandomStringUtils.randomAlphanumeric(5);
    private StatusEnum status = StatusEnum.pending;
    private StatusEnum newStatus = StatusEnum.available;

    @BeforeClass
    public void beforeClass() {
        addNewPet();
    }

    @AfterClass
    public void afterClass() {
        deletePetById(petId);
    }


    @Test
    public void getPetsByStatus() {
        given()
                .log().everything()
                .contentType(ContentType.JSON)
                .queryParam("status", StatusEnum.available.toString())
                .when()
                .get(PET_ENDPOINT + "/findByStatus")
                .then()
                .log().body()
                .body("status", hasItem(StatusEnum.available.toString()));

    }

    @Test
    public void getPetById() {
        Response response = given()
                .log().everything()
                .contentType(ContentType.JSON)
                .pathParam("petId", petId)
                .get(PET_ENDPOINT + "/{petId}");

        //response.then().assertThat().body(matchesJsonSchemaInClasspath("schemas/pet.json"));

        response.prettyPrint();

    }

    @Test
    public void uploadPhoto() throws FileNotFoundException {
        File photo = new File("src/test/resources/photo/dog.jpg");
        Response response = given()
                .log().everything()
                .relaxedHTTPSValidation()
                //.contentType(ContentType.BINARY)
                .header("api_key", Authentication.getApiKey())
                .pathParam("petId", petId)
                .multiPart(photo)
                .post(PET_ENDPOINT + "/{petId}/uploadImage");

        response.prettyPrint();
//        String firstPhoto = response.jsonPath().getString("message");
//        Assert.assertTrue(firstPhoto.contains(photo.getName()));
        Assert.assertEquals(response.getStatusCode(), 200);
    }

    @SneakyThrows
    @Test
    public void getPetByIdAndDoCheck() {
        given()
                .log().everything()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .pathParam("petId", petId)
                .get(PET_ENDPOINT + "/{petId}")
                .then()
                .log().body()
                .statusCode(200)
                .body("name", containsString(petName),
                        "id", equalTo(petId),
                        "status", equalTo(status.toString()));
    }

    @Test(dependsOnMethods = "getPetByIdAndDoCheck")
    public void updateExistingPet() {
        given()
                .log().everything()
                .contentType(ContentType.JSON)
                .pathParam("petId", petId)
                .queryParam("name", newPetName)
                .queryParam("status", newStatus)
                .post(PET_ENDPOINT + "/{petId}");

        given()
                .log().everything()
                .contentType(ContentType.JSON)
                .pathParam("petId", petId)
                .get(PET_ENDPOINT + "/{petId}")
                .then()
                .body("name", equalTo(newPetName),
                        "status", equalTo(newStatus.toString()))
                .extract().body().jsonPath()
                .prettyPrint();
    }


    private void addNewPet() {
        given()
                .relaxedHTTPSValidation()
                .log().everything()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body("{\n" +
                        "  \"id\": " + petId + ",\n" +
                        "  \"name\": \"" + petName + "\",\n" +
                        "  \"category\": {\n" +
                        "    \"id\": 1,\n" +
                        "    \"name\": \"Dogs\"\n" +
                        "  },\n" +
                        "  \"photoUrls\": [\n" +
                        "    \"string\"\n" +
                        "  ],\n" +
                        "  \"tags\": [\n" +
                        "    {\n" +
                        "      \"id\": 0,\n" +
                        "      \"name\": \"string\"\n" +
                        "    }\n" +
                        "  ],\n" +
                        "  \"status\": \"" + status + "\"\n" +
                        "}")
                .header("api_key", Authentication.getApiKey())
                .when()
                .post(PET_ENDPOINT)
                .prettyPeek();

        given()
                .log().everything()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .pathParam("petId", petId)
                .header("api_key", Authentication.getApiKey())
                .when()
                .get(PET_ENDPOINT + "/{petId}")
                .prettyPeek()
                .then()
                .body("name", equalTo(petName));
    }


    private void deletePetById(Integer petId) {
        given()
                .log().everything()
                .contentType(ContentType.JSON)
                .header("api_key", Authentication.getApiKey())
                .pathParam("petId", petId)
                .expect().statusCode(200)
                .when()
                .delete(PET_ENDPOINT + "/{petId}");

        Assert.assertEquals(
                given()
                        .log().everything()
                        .contentType(ContentType.JSON)
                        .pathParam("petId", petId)
                        .expect().statusCode(404)
                        .when()
                        .get(PET_ENDPOINT + "/{petId}")
                        .asString(), "Pet not found");
    }


}
