package org.brit.tests;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.microsoft.playwright.APIRequest;
import com.microsoft.playwright.APIRequestContext;
import com.microsoft.playwright.APIResponse;
import com.microsoft.playwright.Playwright;
import com.microsoft.playwright.options.RequestOptions;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.brit.tests.classes.Pet;
import org.brit.tests.classes.StatusEnum;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.brit.tests.actions.pets.PetsActions.PET_ENDPOINT;

public class PWRestApiTests {
    @Test
    public void test() {
        Pet pet = Pet.builder()
                .id(Long.parseLong(RandomStringUtils.randomNumeric(10)))
                .name("MyLittlePet_" + RandomStringUtils.randomAlphanumeric(5))
                .status(StatusEnum.available)
                .build();

        Playwright playwright = Playwright.create(new Playwright.CreateOptions().setEnv(Map.of("PLAYWRIGHT_SKIP_BROWSER_DOWNLOAD", "1")));
        APIRequestContext apiRequestContext = playwright.request().newContext(new APIRequest.NewContextOptions().setBaseURL("https://petstore3.swagger.io/api/v3"));
        RequestOptions requestOptions = RequestOptions.create();

        requestOptions.setQueryParam("status" , StatusEnum.available.toString());
        APIResponse apiResponse = apiRequestContext.get("./v3/pet/findByStatus", requestOptions);
        String s = new String(apiResponse.body());
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Pet[] pets = gson.fromJson(s, Pet[].class);

        Arrays.asList(pets).forEach(System.out::println);
        assertThat(Arrays.asList(pets))
                .extracting(Pet::getStatus)
                .allMatch(p -> p == StatusEnum.available);
    }
}
